<?php
    //TEMPLATES
    //left footer
    $footerLeft = "<div id='help' class='popup' style='display:none;'>
	<h2>Help</h2>
	<div class='closebutton'>Close</div>
	<p>

	</p>
</div>
<div id='opensource' class='popup' style='display:none;'>
	<h2>Open source</h2>
	<div class='closebutton'>Close</div>
	<p>
		The whole project is completely open source and available at <a href='https://bitbucket.org/matthewd673/recluffle'>Bitbucket</a>.<br>
		If you want to help improving the messy project (it\'s based on PHP), feel free to make a pull request. I\'ll also list everyone helping here.
	</p>
</div>
<div id='about' class='popup' style='display:none;'>
	<h2>About</h2>
	<div class='closebutton'>Close</div>
	<p>Want to use Reddit without people knowing you are using it? This page provides you with the known Google interface for your Reddit needs.<br><br>

	Browse a subreddit by entering it into the box ('/r/internetisbeautiful' or '/r/web_design').<br>
	Search for a term as you would on reddit.<br>
	Press the 'I\'m feeling lucky' - button to browse a random subreddit.<br><br>

	Cluffle is your stealth mode to avoid things like suspicious coworkers and classmates.<br>
	Reddit is blocked at work? Cluffle also works as a proxy.<br>
	</p>
</div>";

    //right footer
    $footerRight = "<div class='noselect' id='footer'>
	<a div='opensource'>Help</a>
	<a div='opensource'>Open Source</a>
	<a div='about'>About</a>

	<span id='footerright'>
		<a class='pullright' href='http://www.reddit.com/u/Lutan' target='_blank'>Contact</a>
		<a class='pullright' href='http://proxy.cluffle.com' target=_blank>Proxypage</a>
	</span>
</div>";

    echo $footerLeft;
    echo $footerRight;

?>