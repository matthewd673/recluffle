//show popups based on id of clicked element, clever
$(document).click("click",function(e) {
	div=$(e.target).attr("div");
	if (div) {
		$("#"+div).show();
	}
});
//hide popups
$(document).on("click",".closebutton",function(e) {
	$(".popup").hide();
});
//on search button click
$(document).on("click","#searchbutton",function(e) {
	alert("Search!");
})
//on cluffy button click
$(document).on("click","#cluffybutton",function(e) {
	alert("So am I!");
})
//replace beta notice text
$(window).load(function () {
	var betaNotice = document.getElementById("beta-notice");
	if(window.location.href.indexOf("cluffle-work-matthewd673") !== -1)
	{
		betaNotice.innerHTML = "Hey there, Matt. <a href='http://cluffle.herokuapp.com'>Having fun? :^)</a>";
	}
	else if (window.location.href.indexOf("recluffle-beta.herokuapp.com") !== -1)
	{
		betaNotice.innerHTML = "Welcome to the Cluffle beta! <a href='http://cluffle.herokuapp.com'>Take me back</a>.";
	}
});