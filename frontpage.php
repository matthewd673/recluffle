<?php
    //this becomes index.php later
    function frontpage() {
	global $useragent;
	
//BUILD HOMEPAGE
require_once(__DIR__ . '/templates/homeHead.php'); //i just know i'll get the two confused eventually
echo '<body>

<div class="noselect" id="navbar">
	';
require_once(__DIR__ . "/templates/homeHeader.php");
echo '
</div>
<div id="content">
	<div id="contentlogo">
	</div>
	<form action="index.php">
		<input type="text" id="contentinput" autofocus name="q" autocomplete="off">
		<div class="noselect" class="contentbutton">
			<button class="contentbutton" id="searchbutton">Cluffle Search</button>
			<button class="contentbutton" id="cluffybutton">I&#39;m Feeling Cluffy!</button>
		</div>
	</form>
	<br><br><br> <!-- temporary! -->
	<div class="hint-text">
		<img src="img/circle-icon.png" style="height: 64px">
		<p id="beta-notice">Cluffle is getting an update! <a href="https://recluffle-beta.herokuapp.com">Test the latest build</a>.</p>
	</div>
</div>';
require_once(__DIR__ . "/templates/homeFooter.php");
echo '</body>';
}

    frontpage();

?>