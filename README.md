# What is RE:Cluffle?

Cluffle used to be an awesome way to browse reddit while at work, but sadly the creator [/u/Lutan](http://www.reddit.com/u/Lutan) decided to take it down.

This project is in no way mine, but I want to keep Cluffle around for those dreary hours at the office and am working on improving the app as we speak.

Again, CLUFFLE IS NOT MINE, I am just a former user who wanted to resurrect this glorious project.

## Cluffle is getting a remake!
If you would like to contribute to the future of Cluffle start making pull requests! The whole thing is pretty informal right now.

You can find the latest build of Cluffle here: http://recluffle-beta.herokuapp.com